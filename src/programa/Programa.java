package programa;

import java.io.IOException;
import java.util.Scanner;

import clases.FicherosAccesoAleatorio;
import clases.FicherosSecuenciales;

public class Programa {

	public static void main(String[] args) throws IOException {
		FicherosSecuenciales fs = new FicherosSecuenciales();
		FicherosAccesoAleatorio raf = new FicherosAccesoAleatorio("raf.txt");
		// inicializo las variables
		int select = 0;
		int select1 = 0;
		// inicializo el scanner
		Scanner input = new Scanner(System.in);
		// texto de bienvenida
		System.out.println("  _____                _   _             ______ \r\n"
				+ " |  __ \\              | | (_)           |____  |\r\n"
				+ " | |__) | __ __ _  ___| |_ _  ___ __ _      / / \r\n"
				+ " |  ___/ '__/ _` |/ __| __| |/ __/ _` |    / /  \r\n"
				+ " | |   | | | (_| | (__| |_| | (_| (_| |   / /   \r\n"
				+ " |_|   |_|  \\__,_|\\___|\\__|_|\\___\\__,_|  /_/    \r\n"
				+ "                                                ");
		// creo un bucle para el primer menu
		do {
			// metodo que imprime el primer menu
			menuUno();
			select = input.nextInt();
			// switch del primer menu
			switch (select) {
			// ficheros secuenciales
			case 1:
				do {
					menuSecuencial();
					select1 = input.nextInt();
					switch (select1) {
					case 1:
						fs.crearFichero();
						break;
					case 2:
						fs.leerFichero();
						break;
					case 3:
						fs.buscarPalabra();
						break;
					case 4:
						fs.dibujarFichero();
						break;
					case 5:
						fs.enumerar();
						break;
					case 6:
						fs.lineas();
						break;
					case 7:
						fs.cuadrado();
						break;
					case 8:
						fs.mitad();
						break;

					default:
						break;
					}
				} while (select1 != 0);

				break;
			// ficheros de acceso aleatorio
			case 2:
				do {
					menuAleatorio();
					select1 = input.nextInt();
					switch (select1) {
					case 1:
						raf.rellenarArchivo();
						break;

					case 2:
						raf.visualizarArchivo();
						break;

					case 3:
						raf.modificarArchivo();
						break;

					case 4:
						raf.borrarTexto();
						break;

					case 5:
						raf.enumerar();
						break;

					case 6:
						raf.escribirNumeroAleatorio();
						break;
					case 7:
						raf.helloKitty();
						break;
					case 8:
						raf.meEncantaProgramacio();
						break;
					case 9:
						
						break;
					
					default:
						break;
					}
				} while (select1 != 0);

				break;
			default:
				break;
			}

		} while (select != 0);
		System.out.println("              _ _           \r\n" + "     /\\      | (_)          \r\n"
				+ "    /  \\   __| |_  ___  ___ \r\n" + "   / /\\ \\ / _` | |/ _ \\/ __|\r\n"
				+ "  / ____ \\ (_| | | (_) \\__ \\\r\n" + " /_/    \\_\\__,_|_|\\___/|___/");
		input.close();
	}

	public static void menuUno() {
		System.out.println("************************************************************************************");
		System.out.println("                                 MENU                                              *");
		System.out.println("1.- ficheros secuenciales                                                          *");
		System.out.println("2.- Ficheros de acceso aleatorio                                                   *");
		System.out.println("0.- Salir                                                                          *");
		System.out.println("************************************************************************************");
	}

	public static void menuSecuencial() {
		System.out.println("************************************************************************************");
		System.out.println("                                 MENU                                              *");
		System.out.println("1.- Crear fichero secuencial                                                       *");
		System.out.println("2.- Visualizar fichero secuencial                                                  *");
		System.out.println("3.- Buscar en fichero secuencial                                                   *");
		System.out.println("4.- Acción extra nº1: Dibujar en el fichero                                        *");
		System.out.println("5.- Acción extra nº2: Enumerar las lineas                                          *");
		System.out.println("6.- Acción extra nº3: Escribir el n de lineas                                      *");
		System.out.println("7.- Acción extra nº4: Cuadrado con el tamaño de lineas                             *");
		System.out.println("8.- Acción extra nº5: Eliminar el 50% del contenido                                *");
		System.out.println("0.- Salir                                                                          *");
		System.out.println("************************************************************************************");
	}

	public static void menuAleatorio() {
		System.out.println("************************************************************************************");
		System.out.println("                                 MENU                                              *");
		System.out.println("1.- Escribir en fichero aleatorio                                                  *");
		System.out.println("2.- Visualizar fichero aleatorio                                                   *");
		System.out.println("3.- Modificar fichero aleatorio                                                    *");
		System.out.println("4.- Acción extra nº1: borrar un texto                                              *");
		System.out.println("5.- Acción extra nº2: escribir una cadena de numeros                               *");
		System.out.println("6.- Acción extra nº3: escribir numero aleatorio del 1 al 100                       *");
		System.out.println("7.- Acción extra nº4: dibujar una carita                                           *");
		System.out.println("8.- Acción extra nº5: cambiar una texto por Me encanta programar                   *");
		System.out.println("0.- Salir                                                                          *");
		System.out.println("************************************************************************************");
	}

}
