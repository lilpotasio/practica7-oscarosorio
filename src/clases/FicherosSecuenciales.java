package clases;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Vector;

public class FicherosSecuenciales {
	private Scanner input;
	private Scanner input2;
	private Scanner input3;
	private Scanner input7;

	// creo un metodo para crear el fichero y si ya existe
	public void crearFichero() {
		File fichero = new File("fichero.txt");

		String linea;
		PrintWriter escritor = null;
		FileWriter escritor1 = null;
		input = new Scanner(System.in);

		try {
			escritor1 = new FileWriter(fichero, true);
			escritor = new PrintWriter(escritor1);
			System.out.println("Escribe * para parar de escribir");
			linea = input.nextLine();
			while (!linea.equalsIgnoreCase("*")) {
				escritor.println(linea);
				linea = input.nextLine();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (escritor != null) {
				escritor.close();
			}
		}
	}

	// metodo para leer el fichero
	public void leerFichero() {
		File fichero = new File("fichero.txt");
		Scanner input = null;
		try {
			input = new Scanner(fichero);
			while (input.hasNextLine()) {
				String cadena = input.nextLine();
				System.out.println(cadena);
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (input != null) {
				input.close();
			}
		}

	}
	//metodo que pide la ruta del archivo el cual usare en otros metodos
	public String pedirRuta() {
		input2 = new Scanner(System.in);
		System.out.println("Escribe la ruta del fichero");
		return input2.nextLine();
	}

	//Acción extra nº1:metodo que dibuja en el fichero
	public void dibujarFichero() {
		File fichero = new File("fichero.txt");

		PrintWriter escritor = null;
		FileWriter escritor1 = null;
		try {
			escritor1 = new FileWriter(fichero, true);
			escritor = new PrintWriter(escritor1);
			escritor.println(dibujosMenu());

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (escritor != null) {
				escritor.close();
			}
		}
	}
	//menu del anterior metodo
	public String dibujosMenu() {
		int select;
		String dibujo = null;
		input3 = new Scanner(System.in);
		System.out.println("Elige un dibujo");
		System.out.println("1- Alien 1");
		System.out.println("2- Alien 2");
		System.out.println("3- Alien 3");
		System.out.println("4- Shrek1");
		System.out.println("5- Shrek2");
		System.out.println("6- Shrek3");
		System.out.println("7- Larry");
		System.out.println("8- Linux");
		System.out.println("9- Sapo");
		System.out.println("10- Bender");
		System.out.println("11- Fry");

		select = input3.nextInt();
		switch (select) {
		case 1:
			dibujo = ("                _____\r\n" + "             ,-\"     \"-.\r\n" + "            / o       o \\\r\n"
					+ "           /   \\     /   \\\r\n" + "          /     )-\"-(     \\\r\n"
					+ "         /     ( 6 6 )     \\\r\n" + "        /       \\ \" /       \\\r\n"
					+ "       /         )=(         \\\r\n" + "      /   o   .--\"-\"--.   o   \\\r\n"
					+ "     /    I  /  -   -  \\  I    \\\r\n" + " .--(    (_}y/\\       /\\y{_)    )--.\r\n"
					+ "(    \".___l\\/__\\_____/__\\/l___,\"    )\r\n" + " \\                                 /\r\n"
					+ "  \"-._      o O o O o O o      _,-\"\r\n" + "      `--Y--.___________.--Y--'\r\n"
					+ "         |==.___________.==| \r\n" + "         `==.___________.==' ");
			break;
		case 2:
			dibujo = (".     .       .  .   . .   .   . .    +  .\r\n"
					+ "  .     .  :     .    .. :. .___---------___.\r\n"
					+ "       .  .   .    .  :.:. _\".^ .^ ^.  '.. :\"-_. .\r\n"
					+ "    .  :       .  .  .:../:            . .^  :.:\\.\r\n"
					+ "        .   . :: +. :.:/: .   .    .        . . .:\\\r\n"
					+ " .  :    .     . _ :::/:               .  ^ .  . .:\\\r\n"
					+ "  .. . .   . - : :.:./.                        .  .:\\\r\n"
					+ "  .      .     . :..|:                    .  .  ^. .:|\r\n"
					+ "    .       . : : ..||        .                . . !:|\r\n"
					+ "  .     . . . ::. ::\\(                           . :)/\r\n"
					+ " .   .     : . : .:.|. ######              .#######::|\r\n"
					+ "  :.. .  :-  : .:  ::|.#######           ..########:|\r\n"
					+ " .  .  .  ..  .  .. :\\ ########          :######## :/\r\n"
					+ "  .        .+ :: : -.:\\ ########       . ########.:/\r\n"
					+ "    .  .+   . . . . :.:\\. #######       #######..:/\r\n"
					+ "      :: . . . . ::.:..:.\\           .   .   ..:/\r\n"
					+ "   .   .   .  .. :  -::::.\\.       | |     . .:/\r\n"
					+ "      .  :  .  .  .-:.\":.::.\\             ..:/\r\n"
					+ " .      -.   . . . .: .:::.:.\\.           .:/\r\n"
					+ ".   .   .  :      : ....::_:..:\\   ___.  :/\r\n"
					+ "   .   .  .   .:. .. .  .: :.:.:\\       :/\r\n"
					+ "     +   .   .   : . ::. :.:. .:.|\\  .:/|\r\n" + "     .         +   .  .  ...:: ..|  --.:|\r\n"
					+ ".      . . .   .  .  . ... :..:..\"(  ..)\"\r\n"
					+ " .   .       .      :  .   .: ::/  .  .::\\");
			break;
		case 3:
			dibujo = ("                                .do-\"\"\"\"\"'-o..                         \r\n"
					+ "                             .o\"\"            \"\"..                       \r\n"
					+ "                           ,,''                 ``b.                   \r\n"
					+ "                          d'                      ``b                   \r\n"
					+ "                         d`d:                       `b.                 \r\n"
					+ "                        ,,dP                         `Y.               \r\n"
					+ "                       d`88                           `8.               \r\n"
					+ " ooooooooooooooooood888`88'                            `88888888888bo, \r\n"
					+ "d\"\"\"    `\"\"\"\"\"\"\"\"\"\"\"\"Y:d8P                              8,          `b \r\n"
					+ "8                    P,88b                             ,`8           8 \r\n"
					+ "8                   ::d888,                           ,8:8.          8 \r\n"
					+ ":                   dY88888                           `' ::          8 \r\n"
					+ ":                   8:8888                               `b          8 \r\n"
					+ ":                   Pd88P',...                     ,d888o.8          8 \r\n"
					+ ":                   :88'dd888888o.                d8888`88:          8 \r\n"
					+ ":                  ,:Y:d8888888888b             ,d88888:88:          8 \r\n"
					+ ":                  :::b88d888888888b.          ,d888888bY8b          8 \r\n"
					+ "                    b:P8;888888888888.        ,88888888888P          8 \r\n"
					+ "                    8:b88888888888888:        888888888888'          8 \r\n"
					+ "                    8:8.8888888888888:        Y8888888888P           8 \r\n"
					+ ",                   YP88d8888888888P'          \"\"888888\"Y            8 \r\n"
					+ ":                   :bY8888P\"\"\"\"\"''                     :            8 \r\n"
					+ ":                    8'8888'                            d            8 \r\n"
					+ ":                    :bY888,                           ,P            8 \r\n"
					+ ":                     Y,8888           d.  ,-         ,8'            8 \r\n"
					+ ":                     `8)888:           '            ,P'             8 \r\n"
					+ ":                      `88888.          ,...        ,P               8 \r\n"
					+ ":                       `Y8888,       ,888888o     ,P                8 \r\n"
					+ ":                         Y888b      ,88888888    ,P'                8 \r\n"
					+ ":                          `888b    ,888888888   ,,'                 8 \r\n"
					+ ":                           `Y88b  dPY888888OP   :'                  8 \r\n"
					+ ":                             :88.,'.   `' `8P-\"b.                   8 \r\n"
					+ ":.                             )8P,   ,b '  -   ``b                  8 \r\n"
					+ "::                            :':   d,'d`b, .  - ,db                 8 \r\n"
					+ "::                            `b. dP' d8':      d88'                 8 \r\n"
					+ "::                             '8P\" d8P' 8 -  d88P'                  8 \r\n"
					+ "::                            d,' ,d8'  ''  dd88'                    8 \r\n"
					+ "::                           d'   8P'  d' dd88'8                     8 \r\n"
					+ " :                          ,:   `'   d:ddO8P' `b.                   8 \r\n"
					+ " :                  ,dooood88: ,    ,d8888\"\"    ```b.                8 \r\n"
					+ " :               .o8\"'\"\"\"\"\"\"Y8.b    8 `\"''    .o'  `\"\"\"ob.           8 \r\n"
					+ " :              dP'         `8:     K       dP''        \"`Yo.        8 \r\n"
					+ " :             dP            88     8b.   ,d'              ``b       8 \r\n"
					+ " :             8.            8P     8\"\"'  `\"                 :.      8 \r\n"
					+ " :            :8:           :8'    ,:                        ::      8 \r\n"
					+ " :            :8:           d:    d'                         ::      8 \r\n"
					+ " :            :8:          dP   ,,'                          ::      8 \r\n"
					+ " :            `8:     :b  dP   ,,                            ::      8 \r\n"
					+ " :            ,8b     :8 dP   ,,                             d       8 \r\n"
					+ " :            :8P     :8dP    d'                       d     8       8 \r\n"
					+ " :            :8:     d8P    d'                      d88    :P       8 \r\n"
					+ " :            d8'    ,88'   ,P                     ,d888    d'       8 \r\n"
					+ " :            88     dP'   ,P                      d8888b   8        8 \r\n"
					+ " '           ,8:   ,dP'    8.                     d8''88'  :8        8 \r\n"
					+ "             :8   d8P'    d88b                   d\"'  88   :8        8 \r\n"
					+ "             d: ,d8P'    ,8P\"\"\".                      88   :P        8 \r\n"
					+ "             8 ,88P'     d'                           88   ::        8 \r\n"
					+ "            ,8 d8P       8                            88   ::        8 \r\n"
					+ "            d: 8P       ,:  -hrr-                    :88   ::        8 \r\n"
					+ "            8',8:,d     d'                           :8:   ::        8 \r\n"
					+ "           ,8,8P'8'    ,8                            :8'   ::        8 \r\n"
					+ "           :8`' d'     d'                            :8    ::        8 \r\n"
					+ "           `8  ,P     :8                             :8:   ::        8 \r\n"
					+ "            8, `      d8.                            :8:   8:        8 \r\n"
					+ "            :8       d88:                            d8:   8         8 \r\n"
					+ " ,          `8,     d8888                            88b   8         8 \r\n"
					+ " :           88   ,d::888                            888   Y:        8 \r\n"
					+ " :           YK,oo8P :888                            888.  `b        8 \r\n"
					+ " :           `8888P  :888:                          ,888:   Y,       8 \r\n"
					+ " :            ``'\"   `888b                          :888:   `b       8 \r\n"
					+ " :                    8888                           888:    ::      8 \r\n"
					+ " :                    8888:                          888b     Y.     8, \r\n"
					+ " :                    8888b                          :888     `b     8: \r\n"
					+ " :                    88888.                         `888,     Y     8: \r\n"
					+ " ``ob...............--\"\"\"\"\"'----------------------`\"\"\"\"\"\"\"\"'\"\"\"`'\"\"\"\"\"");
			break;
		case 4:
			dibujo = ("          c,_.--.,y\r\n" + "            7 a.a(\r\n" + "           (   ,_Y)\r\n"
					+ "           :  '---;\r\n" + "       ___.'\\.  - (\r\n" + "     .'\"\"\"S,._'--'_2..,_\r\n"
					+ "     |    ':::::=:::::  \\\r\n" + "     .     f== ;-,---.' T\r\n"
					+ "      Y.   r,-,_/_      |\r\n" + "      |:\\___.---' '---./\r\n" + "      |'`             )\r\n"
					+ "       \\             ,\r\n" + "       ':;,.________.;L\r\n" + "       /  '---------' |\r\n"
					+ "       |              \\\r\n" + "       L---'-,--.-'--,-'\r\n" + "        T    /   \\   Y\r\n"
					+ "        |   Y    ,   |\r\n" + "        |   \\    (   |\r\n" + "        (   )     \\,_L\r\n"
					+ "        7-./      )  `,\r\n" + "snd    /  _(      '._  \\\r\n" + "     '---'           '--'");
			break;
		case 5:
			dibujo = ("                                     .-.\r\n" + "            ,                     .-' ,c'.\r\n"
					+ "         __rK                    _)a  7  ;\r\n"
					+ "        /  ~,)                  (_,      (\r\n"
					+ "      _;   /a(                   |_.    :'\\\r\n"
					+ "      L/\\.'__/                   \\       ' )nnnK-.\r\n"
					+ "      S  / (_                  .- L,-'   .dHHHb   |\r\n"
					+ "      S( '\\_\\\\                / dHb'----'dHHHHb    \\\r\n"
					+ "      S \\  ,  )      _,-._   / dHHHb\"x.dHHHHHHb     \\\r\n"
					+ "      S |'. '.______/_U/_ '.-z/dHHHp   'dHHHHHb\\     |\r\n"
					+ "     [H |  '..___.--'._C__  ) |         dHHHHHHb\\ _   \\\r\n"
					+ "     /| |_  | \\     L/'--._/_ ;                  k '  /\r\n"
					+ "     |//- '-. ---.__         '|                 /     |\r\n"
					+ "      (       '-.   '.        |               _'-.  _/\r\n"
					+ "  ..\"' `.,  _ ,  :  | \\      _\\             ,/ ,  '/\r\n"
					+ ".\"       ':   .     : |   .-' '',          : |/(/\\]/\r\n"
					+ "          \\  /:  '  | :  /_      '...... .'/      |\r\n"
					+ "           |     |  : / .' '--.__,     __.'\\      /\r\n"
					+ "           |   : ;  |/ |         '----'L,  |     /\r\n"
					+ "            \\  : .   \\  '-.________ /   ]  |____/\r\n"
					+ "    snd     L_____'..'           _.7' _/  <,    >\r\n"
					+ "                                <___.'     /    \\\r\n"
					+ "                                           \\____/");
			break;
		case 6:
			dibujo = ("######################################################################################\r\n"
					+ "#                                                                                    # \r\n"
					+ "#                            ,.--------._                                            #\r\n"
					+ "#                           /            ''.                                         #\r\n"
					+ "#                         ,'                \\     |\"\\                /\\          /\\  #\r\n"
					+ "#                /\"|     /                   \\    |__\"              ( \\\\        // ) #\r\n"
					+ "#               \"_\"|    /           z#####z   \\  //                  \\ \\\\      // /  #\r\n"
					+ "#                 \\\\  #####        ##------\".  \\//                    \\_\\\\||||//_/   #\r\n"
					+ "#                  \\\\/-----\\     /          \".  \\                      \\/ _  _ \\     #\r\n"
					+ "#                   \\|      \\   |   ,,--..       \\                    \\/|(O)(O)|     #\r\n"
					+ "#                   | ,.--._ \\  (  | ##   \\)      \\                  \\/ |      |     #\r\n"
					+ "#                   |(  ##  )/   \\ `-....-//       |///////////////_\\/  \\      /     #\r\n"
					+ "#                     '--'.\"      \\                \\              //     |____|      #\r\n"
					+ "#                  /'    /         ) --.            \\            ||     /      \\     #\r\n"
					+ "#               ,..|     \\.________/    `-..         \\   \\       \\|     \\ 0  0 /     #\r\n"
					+ "#            _,##/ |   ,/   /   \\           \\         \\   \\       U    / \\_//_/      #\r\n"
					+ "#          :###.-  |  ,/   /     \\        /' \"\"\\      .\\        (     /              #\r\n"
					+ "#         /####|   |   (.___________,---',/    |       |\\=._____|  |_/               #\r\n"
					+ "#        /#####|   |     \\__|__|__|__|_,/             |####\\    |  ||                #\r\n"
					+ "#       /######\\   \\      \\__________/                /#####|   \\  ||                #\r\n"
					+ "#      /|#######`. `\\                                /#######\\   | ||                #\r\n"
					+ "#     /++\\#########\\  \\                      _,'    _/#########\\ | ||                #\r\n"
					+ "#    /++++|#########|  \\      .---..       ,/      ,'##########.\\|_||  Donkey By     #\r\n"
					+ "#   //++++|#########\\.  \\.              ,-/      ,'########,+++++\\\\_\\\\ Hard'96       #\r\n"
					+ "#  /++++++|##########\\.   '._        _,/       ,'######,''++++++++\\                  #\r\n"
					+ "# |+++++++|###########|       -----.\"        _'#######' +++++++++++\\                 #\r\n"
					+ "# |+++++++|############\\.     \\\\     //      /#######/++++ S@yaN +++\\                #\r\n"
					+ "#      ________________________\\\\___//______________________________________         #\r\n"
					+ "#     / ____________________________________________________________________)        #\r\n"
					+ "#    / /              _                                             _                #\r\n"
					+ "#    | |             | |                                           | |               #\r\n"
					+ "#     \\ \\            | | _           ____           ____           | |  _            #\r\n"
					+ "#      \\ \\           | || \\         / ___)         / _  )          | | / )           #\r\n"
					+ "#  _____) )          | | | |        | |           (  __ /          | |< (            #\r\n"
					+ "# (______/           |_| |_|        |_|            \\_____)         |_| \\_)           #\r\n"
					+ "#                                                                           19.08.02 #\r\n"
					+ "######################################################################################");
			break;
		case 7:
			dibujo = ("          .sd$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$;             \r\n"
					+ " [bug]  .d$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$             \r\n"
					+ "      .d$$$$$$$$$$P*\"'   `\"*T$$$$$$$$$$$$$$$             \r\n"
					+ "     s$$$$$$$$$P*             `*T$$$$$$$$$$$             \r\n"
					+ "    d$$$$$$$$P'                  `*T$$$$$$$P             \r\n"
					+ "   d$$$$$$$P'                       `T$$$$P              \r\n"
					+ "  d$$$$$$P'                           `T$P \\             \r\n"
					+ " d$$$$$P'                                   \\            \r\n"
					+ ".$$$$$'                 .*\"*                 .           \r\n"
					+ ":$$$$;                  .*\"*-.                           \r\n"
					+ "$$$$$'                 /                      `          \r\n"
					+ "$$$$$.                  .s$$s.    `*.         :          \r\n"
					+ "$$$$$;                 d$$$$$$b      ;     .s$s.         \r\n"
					+ "$$$$$$b.              d$$$$$$$$b     ;    d$$$$$b        \r\n"
					+ "$$$$$$$$$bs._        d$$$$P^^T$$b   /    d$P\"\"\"T$b       \r\n"
					+ "$$$$$$$$$$$$$$bs+=- .$$P*'    `TP       dP     .`T       \r\n"
					+ "$$$$$$$$$$$$$$$$P'  :P'    __          /                 \r\n"
					+ "$$$$$$$$$$$$$$$P    $    .'  `.       /`-.    .          \r\n"
					+ "T$$$$$$$$$$$$$$     :   /      \\     .    `.             \r\n"
					+ " T$$$$$$$$$$$$$        :        ;           \\.           \r\n"
					+ "  T*'   `*^$$$$        |+*\"$P*sss*\"  :*\"$P*ss*\"          \r\n"
					+ " /         `T$$        |   Tbd$P     :  Tbd$P            \r\n"
					+ ";  ._        T$        |    T$$P     :   T$P      _._    \r\n"
					+ "     `\"*+.    T       *\"**--._/       \\   /`. .-*\"   `*. \r\n"
					+ "     .*'  `.                  `*       `*---*'          ;\r\n"
					+ "    (                              `.                   |\r\n"
					+ "                   .'                \\                  ;\r\n"
					+ "\\           /     /`*+...___          `-.             .* \r\n"
					+ " `-._   _.+'          `*. __\"\"\"****------`*-.____.+*\"'   \r\n"
					+ "     \"*\"   \\             `. \"\"\"****------**\"/;           \r\n"
					+ "            `.         \\   `--..._______...'/            \r\n"
					+ "              `*--..___.`.                 /             \r\n"
					+ "                          `*-...______..-*'");
			break;
		case 8:
			dibujo = ("            .-\"\"\"-.\r\n" + "           '       \\\r\n" + "          |,.  ,-.  |\r\n"
					+ "          |()L( ()| |\r\n" + "          |,'  `\".| |\r\n" + "          |.___.',| `\r\n"
					+ "         .j `--\"' `  `.\r\n" + "        / '        '   \\\r\n"
					+ "       / /          `   `.\r\n" + "      / /            `    .\r\n"
					+ "     / /              l   |\r\n" + "    . ,               |   |\r\n"
					+ "    ,\"`.             .|   |\r\n" + " _.'   ``.          | `..-'l\r\n"
					+ "|       `.`,        |      `.\r\n" + "|         `.    __.j         )\r\n"
					+ "|__        |--\"\"___|      ,-'\r\n" + "   `\"--...,+\"\"\"\"   `._,.-' mh");
			break;
		case 9:
			dibujo = ("      ,'``.._   ,'``.\r\n" + "     :,--._:)\\,:,._,.:       All Glory to\r\n"
					+ "     :`--,''   :`...';\\      the HYPNO TOAD!\r\n" + "      `,'       `---'  `.\r\n"
					+ "      /                 :\r\n" + "     /                   \\\r\n"
					+ "   ,'                     :\\.___,-.\r\n" + "  `...,---'``````-..._    |:       \\\r\n"
					+ "    (                 )   ;:    )   \\  _,-.\r\n"
					+ "     `.              (   //          `'    \\\r\n"
					+ "      :               `.//  )      )     , ;\r\n"
					+ "    ,-|`.            _,'/       )    ) ,' ,'\r\n"
					+ "   (  :`.`-..____..=:.-':     .     _,' ,'\r\n"
					+ "    `,'\\ ``--....-)='    `._,  \\  ,') _ '``._\r\n"
					+ " _.-/ _ `.       (_)      /     )' ; / \\ \\`-.'\r\n"
					+ "`--(   `-:`.     `' ___..'  _,-'   |/   `.)\r\n" + "    `-. `.`.``-----``--,  .'\r\n"
					+ "      |/`.\\`'        ,','); SSt\r\n" + "          `         (/  (/");
			break;
		case 10:
			dibujo = ("                 .-.\r\n" + "                (   )\r\n" + "                 '-'\r\n"
					+ "                 J L\r\n" + "                 | |\r\n" + "                J   L\r\n"
					+ "                |   |\r\n" + "               J     L\r\n" + "             .-'.___.'-.\r\n"
					+ "            /___________\\\r\n" + "       _.-\"\"'           `bmw._\r\n"
					+ "     .'                       `.\r\n" + "   J                            `.\r\n"
					+ "  F                               L\r\n" + " J                                 J\r\n"
					+ "J                                  `\r\n" + "|                                   L\r\n"
					+ "|                                   |\r\n" + "|                                   |\r\n"
					+ "|                                   J\r\n" + "|                                    L\r\n"
					+ "|                                    |\r\n" + "|             ,.___          ___....--._\r\n"
					+ "|           ,'     `\"\"\"\"\"\"\"\"'           `-._\r\n"
					+ "|          J           _____________________`-.\r\n"
					+ "|         F         .-'   `-88888-'    `Y8888b.`.\r\n"
					+ "|         |       .'         `P'         `88888b \\\r\n"
					+ "|         |      J       #     L      #    q8888b L\r\n"
					+ "|         |      |             |           )8888D )\r\n"
					+ "|         J      \\             J           d8888P P\r\n"
					+ "|          L      `.         .b.         ,88888P /\r\n"
					+ "|           `.      `-.___,o88888o.___,o88888P'.'\r\n"
					+ "|             `-.__________________________..-'\r\n"
					+ "|                                    |\r\n" + "|         .-----.........____________J\r\n"
					+ "|       .' |       |      |       |\r\n" + "|      J---|-----..|...___|_______|\r\n"
					+ "|      |   |       |      |       |\r\n" + "|      Y---|-----..|...___|_______|\r\n"
					+ "|       `. |       |      |       |\r\n" + "|         `'-------:....__|______.J\r\n"
					+ "|                                  |\r\n" + " L___                              |\r\n"
					+ "     \"\"\"----...______________....--'");
			break;
		case 11:
			dibujo = ("       .  .\"|\r\n" + "      /| /  |  _.----._ \r\n" + "     . |/  |.-\"        \".  /|\r\n"
					+ "    /                    \\/ |__\r\n" + "   |           _.-\"\"\"/        /\r\n"
					+ "   |       _.-\"     /.\"|     /\r\n" + "    \".__.-\"         \"  |     \\\r\n"
					+ "       |              |       |\r\n" + "       /_      _.._   | ___  /\r\n"
					+ "     .\"  \"\"-.-\"    \". |/.-.\\/\r\n" + "    |    0  |    0  |     / |\r\n"
					+ "    \\      /\\_     _/    \"_/ \r\n" + "     \"._ _/   \"---\"       |  \r\n"
					+ "     /\"\"\"                 |  \r\n" + "     \\__.--                |_ \r\n"
					+ "       )          .        | \". \r\n" + "      /        _.-\"\\        |  \".\r\n"
					+ "     /     _.-\"             |    \".  \r\n" + "    (_ _.-|                  |     |\"-._\r\n"
					+ "      \"    \"--.             .J     |    \"-.\r\n"
					+ "              /\\        _.-\" |      Y      \"-.\r\n"
					+ "             /  \\__..--\"     |      |         \".\r\n"
					+ "            /   |            | _/\\_ |           \\\r\n"
					+ "           /| /\\|            |/    \\|            \\            \r\n"
					+ "          / |/   Y          7                   .\"\\  \r\n"
					+ "         /|      |          |                  /   \\\r\n"
					+ "        / |      |          |                 |     \\  \r\n"
					+ "       |  |      '|         |                 |      \\\r\n"
					+ "       |  |       |         |                 |       \\ \r\n"
					+ "      |'  |       '|        |                 |        \\\r\n"
					+ "      |   |        |        |                  \\        \\ \r\n"
					+ "      |   '|       |        |                   \\        \\\r\n"
					+ "     |'    |       |         |                   \\        \\\r\n"
					+ "     |     |       |         |                    \\        \\\r\n"
					+ "     |    |'      |'         |                    |\".       |\r\n"
					+ "    |'    |       |          |                    |  \\       | \r\n"
					+ "    |     |       |          |                    |   |      |\r\n"
					+ "    |     |       |          |                    |   |       |\r\n"
					+ "   |'     |      |'          |                    |    |       | \r\n"
					+ "   |      '|     |           |                _.-\"|    |       |\r\n"
					+ "   |       |     |           |           __.-\"    |     |      '| \r\n"
					+ "  |'       \\_    |           |   __..--\"\"      __.|     |       |\r\n"
					+ "  |        ( \"\"--|-..____...--|\"\"      __..--\"\"  |      |       |\r\n"
					+ "   \\_     _.\"-.__|            |__..--\"\"          |      \\_     _/\r\n"
					+ "   (_\"---\"_.-\"  |                                |      (_\"---\"_)\r\n"
					+ "   / \"---\" |   |'                                |      / \"---\" |\r\n"
					+ "   |       /   |                                 |      |       |\r\n"
					+ "   |\\     |    |                                 |      | /\\  |||\r\n"
					+ "   | | || |   |'                                 |      \\/  | |||\r\n"
					+ "   | | || '|  |            -_                    |          | ||/\r\n"
					+ "   \\ \\\"  \\_| |'              \".                  |          | ||\r\n"
					+ "    \\_|  |   |                |                 |'          `\"´´  \r\n"
					+ "       \\_/  |'                |                 |\r\n"
					+ "            |                /|                |'\r\n"
					+ "            |               | |                |\r\n"
					+ "           |'               | |                |\r\n"
					+ "           |               |' |               |'\r\n"
					+ "          |'              ,|  |               |\r\n"
					+ "          |               |   |               |\r\n"
					+ "          |              ,|   |               |, \r\n"
					+ "         |'              |    '|               |\r\n"
					+ "         |              ,|     |               |\r\n"
					+ "         |              |      '|              '|\r\n"
					+ "         |              |       '|              | \r\n"
					+ "         |             ,|        |,             |\r\n"
					+ "         |             |          |,            |\r\n"
					+ "         |             |           |            |\r\n"
					+ "         |             |           |,           |\r\n"
					+ "         '|            |            |           | \r\n"
					+ "          |            |            |           '|\r\n"
					+ "          |            |            |            |\r\n"
					+ "          |            |,           /            |\r\n"
					+ "         /              |           ( _.-\"\"\"-._   \\ \r\n"
					+ "       _(_              |           /\"/--\"\"-_/ \"-._|\r\n"
					+ " _.--/\"\\  \"--..__       |         /   L_  __/      /\r\n"
					+ "/   (_..--\"\"\"    \"--..__|        |      \"\"     _.-\"|\r\n"
					+ "|_                  __.\"|        |_  __...---\"\"_.-\"\r\n"
					+ "\".\"-______...---\"\"\"\"__.\"         L_\"\"__...---\"\"\r\n"
					+ "  \"-______...---\"\"\"\"               \"\"");
			break;
		default:
			System.out.println("No has seleccionado correctamente");
			break;
		}
		return dibujo;
	}
	//metodo para buscar una palabra en el archivo
	public void buscarPalabra() {
		String ruta = pedirRuta();
		String valorABuscar;
		String linea;
		input7 = new Scanner(System.in);
		System.out.println("BUSCAR PALABRA EN EL VECTOR");
		//pedimos la palabra que queremos buscar
		System.out.println("Introduce la palabra a buscar");
		valorABuscar = input7.nextLine();
		Vector<String> v = new Vector<String>();

		try {
			BufferedReader fuente = new BufferedReader(new FileReader(ruta));
			linea = fuente.readLine();
			while (linea != null) {
				String letra;
				String palabra = "";
				for (int j = 0; j < linea.length(); j++) {
					letra = linea.substring(j, j + 1);
					if (letra.equalsIgnoreCase(" ") == false) {
						palabra = palabra + letra;
					}
				}
				v.add(palabra);
				palabra = "";
				linea = fuente.readLine();
			}
			int cp = 0;
			for (int j = 0; j < v.size(); j++) {
				String valor = v.elementAt(j).toString();
				if (valor.equalsIgnoreCase(valorABuscar) == true) {
					cp++;
					System.out.println("valor encontrado -> " + valorABuscar);
				}
			}
			if (cp == 0) {
				System.out.println("La palabra no se encuentra en el archivo");
			} else {
				//segun cuantas veces se haya encontrado lanzara un mensaje
				if (cp == 1) {
					System.out.println("La palabra se encuentra en el archivo " + cp + " vez");
				} else {
					System.out.println("La palabra se encuentra en el archivo " + cp + " veces");
				}

			}
			// cierro el archivo
			fuente.close();
		} catch (FileNotFoundException e) {
			System.out.println("Fichero no encontrado");
			System.exit(0);
		} catch (IOException e) {
			System.out.println("Error de entrada salida");
			System.exit(0);
		}

	}
	//Acción extra nº2:metodo que enumera las lineas escritas del archivo
	public void enumerar() {
		String linea;
		System.out.println("A continuación se enumeraran las lineas del fichero");
		ArrayList<String> v = new ArrayList<String>();
		try {
			BufferedReader fuente = new BufferedReader(new FileReader("fichero.txt"));
			linea = fuente.readLine();
			while (linea != null) {
				v.add(linea);
				linea = fuente.readLine();
			}
			fuente.close();
			PrintWriter archivo = new PrintWriter(new FileWriter("fichero.txt"));
			for (int i = 0; i <= v.size(); i++) {
				archivo.println(i);
			}
			archivo.close();
		} catch (FileNotFoundException e) {
			System.out.println("Fichero no encontrado");
			System.exit(0);
		} catch (IOException e) {
			System.out.println("Error de entrada salida");
			System.exit(0);
		}

	}
	//Acción extra nº3:metodo que escribe en el archivo el numero de lineas que estan escritas
	public void lineas() {
		String linea;
		System.out.println("");
		ArrayList<String> v = new ArrayList<String>();
		try {
			BufferedReader fuente = new BufferedReader(new FileReader("fichero.txt"));
			linea = fuente.readLine();
			while (linea != null) {
				v.add(linea);
				linea = fuente.readLine();
			}
			fuente.close();
			PrintWriter archivo = new PrintWriter(new FileWriter("fichero.txt"));
			int tamano = v.size();
			archivo.println(tamano);
			archivo.close();
		} catch (FileNotFoundException e) {
			System.out.println("Fichero no encontrado");
			System.exit(0);
		} catch (IOException e) {
			System.out.println("Error de entrada salida");
			System.exit(0);
		}

	}
	//Acción extra nº4:metodo que genera un cuadrado en el archivo del tamaño de las lineas escritas
	public void cuadrado() {
		String linea;
		System.out.println("A continuacion se dibujara un cuadrado del tamaño de las lineas que tenia el archivo");
		ArrayList<String> v = new ArrayList<String>();
		try {
			BufferedReader fuente = new BufferedReader(new FileReader("fichero.txt"));
			linea = fuente.readLine();
			while (linea != null) {
				v.add(linea);
				linea = fuente.readLine();
			}
			fuente.close();
			PrintWriter archivo = new PrintWriter(new FileWriter("fichero.txt"));
			int n = v.size();
			for (int i = 1; i <= n; i++) {
				for (int j = 1; j <= n; j++) {
					archivo.print(" *");
				}
				archivo.println("");
			}
			archivo.close();
		} catch (FileNotFoundException e) {
			System.out.println("Fichero no encontrado");
			System.exit(0);
		} catch (IOException e) {
			System.out.println("Error de entrada salida");
			System.exit(0);
		}

	}
	//Acción extra nº5:metodo que elimina la mitad de las lineas del archivo
	public void mitad() {
		String linea;
		System.out.println("Se va a eliminar el 50% del archivo");
		ArrayList<String> v = new ArrayList<String>();

		try {
			BufferedReader fuente = new BufferedReader(new FileReader("fichero.txt"));
			linea = fuente.readLine();
			while (linea != null) {
				v.add(linea);
				linea = fuente.readLine();
			}
			fuente.close();
			PrintWriter archivo = new PrintWriter(new FileWriter("fichero.txt"));
			for (int i = 0; i < (v.size() / 2); i++) {
				archivo.println(v.get(i));
			}
			archivo.close();
		} catch (FileNotFoundException e) {
			System.out.println("Fichero no encontrado");
			System.exit(0);
		} catch (IOException e) {
			System.out.println("Error de entrada salida");
			System.exit(0);
		}

	}

}
