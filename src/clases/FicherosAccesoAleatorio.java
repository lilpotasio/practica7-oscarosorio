package clases;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.util.Scanner;

import javax.swing.plaf.synth.SynthSpinnerUI;

public class FicherosAccesoAleatorio {
	//creo el string de la ruta del archivo
	String archivo;
	//creo el objeto con la la ruta
	public FicherosAccesoAleatorio(String contenido) {
		this.archivo = contenido;
	}
	//metodo para escribir el el fichero de acceso aleatorio
	public void rellenarArchivo() {
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
			RandomAccessFile f = new RandomAccessFile(archivo, "rw");
			f.seek(f.length());
			String respuesta = "";
			do {
				System.out.println("Escribe ");
				String contenido = in.readLine();
				contenido = modificarContenido(contenido, 20);
				f.writeUTF(contenido);
				System.out.println("¿Deseas continuar (si/no)?");
				respuesta = in.readLine();
			} while (respuesta.equalsIgnoreCase("si"));
			f.close();
		} catch (IOException e) {
			System.out.println("error");
		}
	}
	//metodo para modificar el contenido
	private String modificarContenido(String contenido, int lon) {
		// cambiar el tamaño de la cadena
		if (contenido.length() > lon) {
			return contenido.substring(0, lon);
		} else {
			// completo los espacios
			for (int i = contenido.length(); i < lon; i++) {
				contenido = contenido + " ";
			}
		}
		return contenido;
	}
	//metodo para ver el archivo
	public void visualizarArchivo() {
		try {
			RandomAccessFile f = new RandomAccessFile(archivo, "rw");
			String contenido = "";
			boolean finFichero = false;
			do {
				try {
					contenido = f.readUTF();
					System.out.println(contenido);
				} catch (EOFException e) {
					System.out.println("Fin fichero");
					finFichero = true;
					f.close();
				}

			} while (!finFichero);
		} catch (IOException e) {
			System.out.println("error");
		}
	}
	//metodo para modificar el archivo el cual usare en otros metodos
	public void modificarArchivo() {
		try {			
			BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
			String viejo = "";
			String nuevo = "";
			System.out.println("texto a modificar");
			viejo = in.readLine();
			System.out.println("Dame el nuevo contenido");
			nuevo = in.readLine();
			RandomAccessFile f = new RandomAccessFile(archivo, "rw");
			String contenido;
			boolean finFichero = false;
			boolean valorEncontrado = false;
			do {
				try {
					contenido = f.readUTF();
					if (contenido.trim().equalsIgnoreCase(viejo)) {
						f.seek(f.getFilePointer() - 22);
						nuevo = modificarContenido(nuevo, 20);
						f.writeUTF(nuevo);
						valorEncontrado = true;
					}
				} catch (EOFException e) {
					f.close();
					finFichero = true;
				}
			} while (!finFichero);
			if (valorEncontrado == false) {
				System.out.println("El valor no está en el fichero");
			} else {
				System.out.println("El valor ha sido modificado");
			}
		} catch (IOException e) {
			System.out.println("error");
		}
	}
	//s Acción extra nº1:creo metodo para borrar un contenido determinado
	public void borrarTexto() {
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
			String viejo = "";
			String nuevo = "";
			System.out.println("texto a borrar");
			viejo = in.readLine();
			nuevo = "";
			RandomAccessFile f = new RandomAccessFile(archivo, "rw");
			String contenido;
			boolean finFichero = false;
			boolean valorEncontrado = false;
			do {
				try {
					contenido = f.readUTF();
					if (contenido.trim().equalsIgnoreCase(viejo)) {
						f.seek(f.getFilePointer() - 22);
						nuevo = modificarContenido(nuevo, 20);
						f.writeUTF(nuevo);
						valorEncontrado = true;
					}
				} catch (EOFException e) {
					f.close();
					finFichero = true;
				}
			} while (!finFichero);
			if (valorEncontrado == false) {
				System.out.println("El valor no está en el fichero");
			} else {
				System.out.println("El texto ha sido borrado");
			}
		} catch (IOException e) {
			System.out.println("error");
		}
	}
	//creo metodo para escribit que usare en otros metodos
	public void escribir(String contenido) {
		try {
			RandomAccessFile f = new RandomAccessFile(archivo, "rw");
			f.seek(f.length());
			contenido = modificarContenido(contenido, 20);
			f.writeUTF(contenido);
			f.close();
		} catch (IOException e) {
			System.out.println("error");
		}
	}
	//s Acción extra nº2:creo metodo que genera una cadena de numeros de menor a mayor
	public void enumerar() {
		Scanner input = new Scanner(System.in);
		System.out.println("Hasta que numero quieres escribir?");
		int numero = input.nextInt();
		for (int i = 1; i <= numero; i++) {
			String numeroText = String.valueOf(i);
			escribir(numeroText);
		}
	}
	//s Acción extra nº3:creo un metodo que genera un numero aleatorio del 1 al 100
	public void escribirNumeroAleatorio() {
		System.out.println("se va a escribir en el archivo un numero aleatorio del 1 al 100");
		int numero = (int) Math.floor(Math.random() * 99 + 1);
		String numeroText = String.valueOf(numero);
		escribir(numeroText);
	}
	//s Acción extra nº4:creo un metodo que esribe una carita en el archivo
	public void helloKitty() {
		escribir("( ͡❛ ͜ʖ ͡❛)");
	}
	//s Acción extra nº5:creo un metodo que cambia un palabra determinada por me encanta programar
	public void meEncantaProgramacio() {
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
			String viejo = "";
			String nuevo = "";
			System.out.println("texto a modificar");
			viejo = in.readLine();
			nuevo = "me encanta programar";
			RandomAccessFile f = new RandomAccessFile(archivo, "rw");
			String contenido;
			boolean finFichero = false;
			boolean valorEncontrado = false;
			do {
				try {
					contenido = f.readUTF();
					if (contenido.trim().equalsIgnoreCase(viejo)) {
						// tengo que modificarlo
						// 20+2 (cadena de 20 + 2 bytes) -> cadena + 2 bytes
						f.seek(f.getFilePointer() - 22);
						nuevo = modificarContenido(nuevo, 20);
						f.writeUTF(nuevo);
						valorEncontrado = true;
					}
				} catch (EOFException e) {
					f.close();
					finFichero = true;
				}
			} while (!finFichero);
			if (valorEncontrado == false) {
				System.out.println("El valor no está en el fichero");
			} else {
				System.out.println("Se ha escrito me encanta programacion");
			}
		} catch (IOException e) {
			System.out.println("error");
		}
	}
}